import psycopg2


def main():
    # Define our connection string
    host='localhost'
    dbname='rn_gis'
    user='Vincent'
    password=''

    # print the connection string we will use to connect
    # print("Connecting to database\n    %s" % ())

    # get a connection, if a connect cannot be made an exception will be raised here
    conn = psycopg2.connect(host=host, dbname=dbname, user=user, password=password)

    # conn.cursor will return a cursor object, you can use this cursor to perform queries
    cursor = conn.cursor()
    cursor.execute('select * from sectors limit 10')
    print cursor.fetchone()
    print("Connected!\n")


if __name__ == "__main__":
    main()