import sys
import psycopg2
import json
from ConfigParser import ConfigParser


# Query passed in with function call
def with_args(query):

    # create config parser
    # read config file
    conf = ConfigParser()
    conf.read('config.ini')

    # get connection values from config file
    conn_dict = dict(conf.items('database'))

    # get a connection, if a connect cannot be made an exception will be raised here
    conn = psycopg2.connect(**conn_dict)

    # conn.cursor will return a cursor object, you can use this cursor to perform queries
    cursor = conn.cursor()

    cursor.execute(query)
    print cursor.fetchone()


# Queries from list in config file
def with_config_list():
    # create config parser
    # read config file
    conf = ConfigParser()
    conf.read('config.ini')
    conn_dict = dict(conf.items('database'))

    # pull query section from config file
    query_dict = dict(conf.items('query'))

    # get select query
    # This parses the queries into a list of queries to iterate over
    # Add in multiple values like select, update, etc to split up the queries
    select_query = json.loads(query_dict['select'])

    # get a connection, if a connect cannot be made an exception will be raised here
    conn = psycopg2.connect(**conn_dict)

    # conn.cursor will return a cursor object, you can use this cursor to perform queries
    cursor = conn.cursor()

    # iterates over the select values in the config file
    for q in select_query:
        cursor.execute(q)
        # This fetches one result, but can use fetchall, or pandas
        print cursor.fetchone()


# Query hard coded in function
def with_hardcode():

    query = "select * from sectors limit 10"

    # create config parser
    # read config file
    conf = ConfigParser()
    conf.read('config.ini')

    # get connection values from config file
    conn_dict = dict(conf.items('database'))

    # get a connection, if a connect cannot be made an exception will be raised here
    conn = psycopg2.connect(**conn_dict)

    # conn.cursor will return a cursor object, you can use this cursor to perform queries
    cursor = conn.cursor()

    cursor.execute(query)
    print cursor.fetchone()


# Queries read from sql file
def with_sql_file():

    # create config parser
    # read config file
    conf = ConfigParser()
    conf.read('config.ini')
    conn_dict = dict(conf.items('database'))

    # Example of opening a file and splitting up queries
    with open('query_file.sql', 'r') as f:
        queries = f.read().split('\n')

    # get a connection, if a connect cannot be made an exception will be raised here
    conn = psycopg2.connect(**conn_dict)

    # conn.cursor will return a cursor object, you can use this cursor to perform queries
    cursor = conn.cursor()

    # iterate over queries in a file
    for q in queries:
        cursor.execute(q)
        # This fetches one result, but can use fetchall, or pandas
        print cursor.fetchone()


# executes sample queries
if __name__ == "__main__":

    # if there are enough arguments to run
    if len(sys.argv) > 1:

        # if there's a query passed
        if len(sys.argv) > 2:

            if sys.argv[1] == 'args':
                # get the query
                query = sys.argv[2]

                # run the function
                with_args(query)

        # if config is passed
        if sys.argv[1] == 'config':
            # run from config file
            with_config_list()

        # if hardcode is passed
        if sys.argv[1] == 'hardcode':
            with_hardcode()

        if sys.argv[1] == 'file':
            with_sql_file()

        else:
            print 'bad command line arguments given'

    else:
        print 'no command line arguments given'